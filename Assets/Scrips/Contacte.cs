﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contacte : MonoBehaviour
{
    public void AbrirURL(string URL) {
        Application.OpenURL(URL);
    }
}
