﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PantallaInici : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("PantallaPreguntes");
    }
    public void Opciones()
    {
        SceneManager.LoadScene("PantallaOpcions");
    }
    public void DarrerResultat()
    {
        SceneManager.LoadScene("PantallaResultat");
    }
}
