﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayesStates
{
    public float[] maxResportesPosibles=new float[5];
    public float[] respostes = new float[5];

    public int[] result()
    {
        int[] resultat = new int[5];
        for (int i=0; i < resultat.Length; i++)
        {
            if (maxResportesPosibles[i] == 0)
            {
                resultat[i] = 0;
            }
            else
            {
                resultat[i] = (int)((respostes[i] / maxResportesPosibles[i])*100);
            }
            
        }
        return resultat;
    }
}
