﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TornarInici : MonoBehaviour
{
    public void VolverInicio() {
        SceneManager.LoadScene("PantallaInici");
    }
    
}
