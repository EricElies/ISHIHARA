﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultManage : MonoBehaviour
{
    public Text resultText;
    private int[] resultat;

    void Start()
    {
        if (ResultData.Result!=null) {
            resultat = ResultData.Result;
            resultText.text = ("Visió normal: " + resultat[0].ToString() + "%" +
                "\nProtanopia (Ceguera al verd): " + resultat[1].ToString() + "%" +
                "\nDeuteranopia (Ceguera al vermell): " + resultat[2].ToString() + "%" +
                "\nDiferencia vermell-verd (Desplaçament): " + resultat[3].ToString() + "%" +
                "\nAltres: " + resultat[4].ToString() + "%");
        }
        else
        {
            resultText.text = ("No hi ha darrer resultat.");
        }

    }
    private void Update()
    {
        
    }
}
