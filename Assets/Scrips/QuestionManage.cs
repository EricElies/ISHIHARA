﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Question
{
    public int numQuestion;
    public string pregunta;
    public List<Dropdown.OptionData> options;
    public List<Dropdown.OptionData> resultOptions;
    public Sprite imatge;
}
 public enum OpcionsResposta
{
    Good,
    Protanopia,
    Deuteranopia,
    GreenRed,
    Other,
    Not
}
public class QuestionManage : MonoBehaviour
{
    public Question[] questions;
    public Image imatge;
    public Text pregunta;
    public Dropdown opcions;
    public Text next;
    public int indexQuestion=0;
    public PlayesStates jugador=new PlayesStates();
    // Start is called before the first frame update
    void Start()
    {
        canviPregunta();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
     public void nextQuestion()
    {
        if (opcions.value < questions[indexQuestion].resultOptions.Count) {
            if (indexQuestion == questions.Length - 1)
            {
                addPosibles();
                addResposta();
                finish();
            }
            else
            {
                if (indexQuestion == questions.Length - 2)
                {
                    next.text = "Acabar";
                }
                addPosibles();
                addResposta();
                indexQuestion++;
                canviPregunta();


            }
        }
        
    }
    public void canviPregunta()
    {
        opcions.ClearOptions();
        opcions.AddOptions(questions[indexQuestion].options);
        imatge.sprite = questions[indexQuestion].imatge;
        pregunta.text = questions[indexQuestion].pregunta;
    }
    public void finish()
    {
        ResultData.Result= jugador.result();
        SceneManager.LoadScene("PantallaResultat");

    }
    public void addPosibles()
    {
        bool[] posibles = new bool[5];
        foreach(Dropdown.OptionData element in questions[indexQuestion].resultOptions)
        {
            string elementText = element.text;
            switch (elementText)
            {
                case "Good":
                    posibles[0] = true;
                    break;

                case "Protanopia":
                    posibles[1] = true;
                    break;

                case "Deuteranopia":
                    posibles[2] = true;
                    break;

                case "GreenRed":
                    posibles[3] = true;
                    break;

                case "Other":
                    posibles[4]= true;
                    break;

                case "Not":
                    break;
            }
        }
        for (int i=0; i < posibles.Length; i++)
        {
            if (posibles[i])
            {
                jugador.maxResportesPosibles[i]++;
            }
        }
    }
    public void addResposta()
    {
        
        switch (questions[indexQuestion].resultOptions[opcions.value].text)
        {
            case "Good":
                jugador.respostes[0]++;
                break;

            case "Protanopia":
                jugador.respostes[1]++;
                break;

            case "Deuteranopia":
                jugador.respostes[2]++;
                break;

            case "GreenRed":
                jugador.respostes[3]++;
                break;

            case "Other":
                jugador.respostes[4]++;
                break;

            case "Not":
                break;
        }
    }
}
