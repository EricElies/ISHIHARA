﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Annexe : MonoBehaviour
{
    public void IrAnnexo()
    {
        SceneManager.LoadScene("PantallaAnnexe");
    }
}
